/* 
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Hisham Albeetar    05/12/2018  - First version
 */
package ae.isoft.ems.page.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import ae.isoft.ems.EmsRTA;
import ae.isoft.ems.util.AutomationLogger;
import ae.isoft.ems.util.TestingWrapper;

/**
 * Testing Wrapper class.
 * 
 * @author Hisham Albeetar
 * @version 1.0  05/12/2018
 */

public class LoginPage {

	/**
	 * Initialize Login Page
	 * 
	 */
	public LoginPage(WebDriver driver, String username, String password) {
		this.username = username;
		this.password = password;
		this.driver = driver;
	}
	
	/**
	 * login
	 * 
	 * @throws InterruptedException
	 */
	public void login() throws InterruptedException, Exception {
		try {
			TestingWrapper.waitForElement(By.id("loginForm:username"),driver);
			driver.findElement(By.id("loginForm:username")).sendKeys(username);
			driver.findElement(By.id("loginForm:password")).sendKeys(password); 
			driver.findElement(By.id("loginForm:loginLink")).click();
			TestingWrapper.waitForElement(By.id("loginForm:username"),driver);
			AutomationLogger.Log(LoginPage.class, "success login for username : " + username);
		} catch (InterruptedException e) {
			AutomationLogger.Log(LoginPage.class, "failed login for username : " + username, e);
			throw e;
		}
	}
	
 	/**
 	 * sign Out
 	 */
 	public void signOut() throws InterruptedException {
		try {
	 		TestingWrapper.JSClickElement(By.xpath("//*[@id=\"header\"]/ul/li[6]/ul/li[4]/a"), driver);
	 		TestingWrapper.JSClickElement(By.xpath("//*[@id=\"header\"]/ul/li[6]/ul/li[4]/ul/li[6]/a"), driver);
			AutomationLogger.Log(LoginPage.class, "success signOut : ");
		} catch (InterruptedException e) {
			AutomationLogger.Log(LoginPage.class, "failed to signOut ", e);
			throw e;
		}
  	}    

	/**
	 * Fields
	 */
	
	private String username;
	
	private String password;
	
	private WebDriver driver;


}

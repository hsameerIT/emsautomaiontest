package ae.isoft.ems.page.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import ae.isoft.ems.util.AutomationLogger;
import ae.isoft.ems.util.TestingWrapper;

public class ProfileMenu {
	
	
	private static final String createOutwardXPath = "#sidebar > div > div > ul > li.sub-menu.normal-menu.toggled > ul > li:nth-child(2) > a";
	private static final String rightMenuXPath = "//*[@id=\"menu-trigger\"]";
	private static final String outwardModuleXPath = "#sidebar > div > div > ul > li:nth-child(4)";

	/**
	 * Initialize Login Page
	 * 
	 */
	public ProfileMenu(WebDriver driver) {
		this.driver = driver;
	}
	
 	/**
 	 * toggle the right menu
 	 */
 	private void toggleRightMenu() throws InterruptedException {
 		TestingWrapper.JSClickElement(By.xpath(rightMenuXPath), driver);
		AutomationLogger.Log(LoginPage.class, "success toggle right menu");
  	}   

 	/**
 	 * Open the Outward Module
 	 */
 	private void openOutwardModule() throws InterruptedException,Exception {
 		TestingWrapper.clickElement(By.cssSelector(outwardModuleXPath), driver);
		AutomationLogger.Log(LoginPage.class, "success open Create Module");
  	}   

 	/**
 	 * Open the Create Outward
 	 */
 	public void openCreateOutward() throws InterruptedException, Exception {
		toggleRightMenu();
 		TestingWrapper.clickElement(By.cssSelector(outwardModuleXPath), driver);
 		TestingWrapper.clickElement(By.cssSelector(createOutwardXPath), driver);	
		toggleRightMenu();
		AutomationLogger.Log(LoginPage.class, "success open Create Outward ");
  	}   

	/**
	 * Fields
	 */	
	private WebDriver driver;

}

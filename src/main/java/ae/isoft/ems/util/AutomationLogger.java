/* 
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Hisham Albeetar    05/12/2018  - First version
 */
package ae.isoft.ems.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

/**
 * Automation logger class.
 * 
 * @author Hisham Albeetar
 * @version 1.0  05/12/2018
 */
public abstract class AutomationLogger{
    /*
     * Class variables.
     */
    /**Automation logger object.*/
    private static Logger automationLogger=Logger.getLogger(AutomationLogger.class);
    
    /**List of Loggers.*/
    private static Map loggers=new HashMap();
    static{
        loggers.put(AutomationLogger.class.getName(),automationLogger);
    }
    
    /**
     * Log message.
     * 
     */
    public static void Log(Class clas,String message){
        try{
            if(clas==null){
                clas=AutomationLogger.class;
            }
            getLogger(clas).debug(message);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    /**
     * Log message with exception.
     * 
     */
    public static void Log(Class clas,String message ,Throwable ex){
        try{
            if(clas==null){
                clas=AutomationLogger.class;
            }
            getLogger(clas).debug(message,ex);
        }catch(Exception e){
            e.printStackTrace();
        }
    }  
    
    /**
     * Get logger. If the class has previously logger object we will return it 
     * by using Logger.getLogger(className) but if this method return null we will 
     * retreive isoft logger object.
     * 
     * @return loggger object.
     * @param clas class name.
     */
    private static Logger getLogger(Class clas){
        String className=clas.getName();
        if(loggers.containsKey(className)){
            return (Logger) loggers.get(className);
        }
        Logger temp=Logger.getLogger(clas);
        if(temp==null){
            return (Logger) loggers.get(AutomationLogger.class.getName());
        }
        loggers.put(clas.getName(),temp);
        return temp;
    }
}
/**
 * Copyright : IdealSoft Emirates L.L.C. (c) 2007
 * Author    : Hamzeh Abu Lawi
 * Date      : 13-02-2007
 * Comment   : For any developer who update this class please document you work in 
 *             the class history
 * 
 * =========================== Class History ==============================
 *       Name                   Date                        Comments
 * -------------------     ----------------------    ----------------------
 * Hamzeh Abu Lawi          13-02-2007                  First version
 * 
 * 
 * 
 **/
package ae.isoft.ems.util;

import java.util.*;

public class Config  
{
    private static HashMap map =new HashMap();
    public static final String  SYSTEM_URL  ="SYSTEM_URL";

    static 
    {
        ResourceBundle  resBun=ResourceBundle.getBundle("config");
        Enumeration params=resBun.getKeys();
        while(params.hasMoreElements()) 
        {
            Object obj=params.nextElement();
            map.put(obj,resBun.getString(obj.toString()));
        }//End while
        setMap(map);
    }
    public Config() 
    {
        
    }    
    
    public static String getStringProperty(String propName)
    {
        Object obj=getMap().get(propName);
        return (obj==null?"":obj.toString());
    }


    private static void setMap(HashMap newMap) 
    {
        map=newMap;
    }

    public static HashMap getMap() 
    {
        return map;
    }
    
}
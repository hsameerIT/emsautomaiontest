/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ae.isoft.ems.util;

import com.api.DBConnection;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author scorpion's bar
 */
public class DBMenager {

    private static Connection conn;
    private Statement statement;
    
    /**
     * 
     * @return
     */
	public static Connection openConnection() {
		try {
		if(conn == null || conn.isClosed()) {
			Class.forName("oracle.jdbc.driver.OracleDriver"); 
			conn = DriverManager.getConnection("jdbc:oracle:thin:@172.18.125.48:1521:EMS12C", "ems", "ems");
		    }         
		} catch (Exception e) {
			e.printStackTrace();
			}
	return conn;
	}

    /**
     * 
     */
    public static void closeConnection() {
        try {
        	if(conn !=null && !conn.isClosed()) {
        			conn.close();
            }         
        } catch (Exception e) {
           e.printStackTrace();
        }
    }

	
}

/* 
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Hisham Albeetar    05/12/2018  - First version
 */
package ae.isoft.ems.util;

import java.awt.AWTException;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.BasicConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ae.isoft.ems.EmsRTA;

/**
 * Test Managed Bean class.
 * 
 * @author Hisham Albeetar
 * @version 1.0  05/12/2018
 */

public abstract class TestManagedBean {

	/**
	 * Abstract Methods
	 */
	
	
	
	@BeforeTest
	public abstract void beforeTest();
	/**
	 * Main Attributes
	 */
	protected WebDriver driver;
	protected WebDriverWait wait;

	/**
	 * Common Public Business Methods
	 */
	

	/**
	 * This method to initialize chrome browser 
	 */
	public void chromeInitialize() {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
			options.addArguments("disable-extensions");
			options.addArguments("--start-maximized");
		    driver = new ChromeDriver(options);
			driver.manage().deleteAllCookies();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
	}
	
	/**
	 * This method to start  the test scenario 
	 */
	public void startTest() {
		driver.get(Config.getStringProperty(Config.SYSTEM_URL));
		BasicConfigurator.configure();
		AutomationLogger.Log(EmsRTA.class, "start");
	}

	/**
	 * Helper Methods
	 */
}

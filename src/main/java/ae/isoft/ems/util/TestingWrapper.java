/* 
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Hisham Albeetar    05/12/2018  - First version
 */

package ae.isoft.ems.util;

import java.awt.AWTException;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.google.common.base.Predicate;

/**
 * Testing Wrapper class.
 * 
 * @author Hisham Albeetar
 * @version 1.0  05/12/2018
 */

public class TestingWrapper {

	 /**
	  *  Login to the system
	 * @throws Exception 
	  */
	public static void emsLogin(WebDriver driver, String username, String password) throws Exception {
		waitForElement(By.id("loginForm:username"),driver);
		driver.findElement(By.id("loginForm:username")).sendKeys(username);
		driver.findElement(By.id("loginForm:password")).sendKeys(password); 
		driver.findElement(By.id("loginForm:loginLink")).click();
		
	}
	
	/**
	 * MainMethods.selectFromFTFList(xpath for lookup div,xpath for the input inside the lookup div, the value ypu want to write inside, driver);
	 * @param componentLocator xpath for list
	 * @param locator xpath for field
	 * @param value i need it 
	 * @param driver
	 * @throws Exception 
	 */
	public static void selectFromSingleLookup(By componentLocator,By locator, String value, WebDriver driver) throws Exception
	{
		clickElement(componentLocator, driver);
		driver.findElement(locator).sendKeys(value);
		driver.findElement(locator).sendKeys(Keys.chord(Keys.RETURN));
		Thread.sleep(1000);
	}
	
	/**
	 * 
	 * @param componentLocator
	 * @param locator
	 * @param value
	 * @param driver
	 * @throws Exception 
	 */
	public static void selectFromAdvanceLookUp(String componentId, String value, WebDriver driver) throws Exception
	{
		By componentLocator = By.xpath("//*[contains(@id, '" + componentId + ":component')]/div[1]/div[1]/div[2]/div[1]");
		By locator = By.xpath("//*[contains(@id, '" + componentId + ":field-selectized')]");
		clickElement(componentLocator, driver);
		driver.findElement(locator).sendKeys(value);
        //waitForItem("'" + componentId + ":component'", driver, value, itemOrder);
		Thread.sleep(3000);
		driver.findElement(locator).sendKeys(Keys.chord(Keys.RETURN));
	}
	
	/**
	 * @throws Exception 
	 * 
	 */
	public static void clickElement(By locator,WebDriver driver) throws Exception {
		int trials=0;
		while (trials < 5) {
			try {
				waitForElement(locator, driver);
				try {
					driver.findElement(locator).click();
				}catch (org.openqa.selenium.ElementNotVisibleException ex) {
					List<WebElement> elementsList =  driver.findElements(locator);
					elementsList.get(elementsList.size()-1).click();
				}
				break;
			} catch (Exception ex) {
				if(trials >= 5) {
					throw ex;
				}
				Thread.sleep(1000);
			}
		}

	}
	
	/**
	 * @throws InterruptedException 
	 * 
	 */
	public static void JSClickElement(By locator,WebDriver driver) throws InterruptedException {
		int trials=0;
		while (trials < 5) {
			try {
				((JavascriptExecutor)driver).executeScript("arguments[0].click();", driver.findElement(locator));
				break;
			} catch (Exception ex) {
				if(trials >= 5) {
					throw ex;
				}
				Thread.sleep(1000);
			}
		}

	}
	
	public static void captureScreenShot(WebDriver driver, String screenshotName) throws IOException {

		TakesScreenshot ts = (TakesScreenshot) driver;
		File imageSource = ts.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(imageSource, new File("./Screenshots/" + screenshotName + ".png"));

	}
	/**
	 * 
	 * @param by
	 * @throws Exception 
	 */
	public static void waitForElement(By by, WebDriver driver) throws Exception {
		seconds=0;
		while (seconds < 60) {
			try {
				driver.findElement(by).isDisplayed();
				if(!driver.findElement(by).isEnabled())
					throw new Exception();
				return;
			} catch (Exception e) {
				Thread.sleep(1000);
				seconds++;
				if(seconds >= 60) {
					throw e;
				}
			}
		}
	}
	
	/**
	 * 
	 * @param by
	 * @throws Exception 
	 */
	public static void waitForItem(String parentComponentId,WebDriver driver, String text, int itemOrder) throws Exception {
		seconds=0;
		while (seconds < 60) {
			try {
				By locator = By.xpath("//*[contains(@id, " + parentComponentId + ")]" + "/div[1]/div[1]/div[2]/div[1]/div" + (itemOrder == 1 ? "" : "[" + itemOrder + "]") + "/div[2]/div[1]");
				driver.findElement(locator).isDisplayed();
				if(!driver.findElement(locator).isEnabled())
					throw new Exception();
				String innerText = driver.findElement(locator).getText();
				if(innerText == null || !innerText.contains(text))
					throw new Exception();
				return;
			} catch (Exception e) {
				Thread.sleep(1000);
				seconds++;
				if(seconds >= 60) {
					throw e;
				}
			}
		}
	}

    //File upload by Robot Class
    public static void uploadFileWithRobot (WebDriver driver, String imagePath, String componentId, int itemOrder) throws Exception {
        clickElement(By.xpath("//*[contains(@id, '" + componentId + ":controlButton:submitButton:actionButton:component')]/button"), driver);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(new StringSelection(System.getProperty("user.dir") + "\\" + imagePath), null);
        Robot robot = null;
        try {
            robot = new Robot();
            robot.delay(250);
            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_V);
            robot.keyRelease(KeyEvent.VK_V);
            robot.keyRelease(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_ENTER);
            robot.delay(150);
            robot.keyRelease(KeyEvent.VK_ENTER);
            String fileName = imagePath.contains("\\") ? imagePath.substring(imagePath.lastIndexOf("\\"), imagePath.length()-4) : imagePath;
            waitForItem("'" + componentId + ":component'", driver, fileName, itemOrder);
        } catch (AWTException e) {
            throw e;
        }
    }
    
    public static boolean isBlankOrNull(Object value) {
    	return ((value == null) || (value.toString().trim().length() == 0)) ||
    			(value.toString().trim().equalsIgnoreCase("undefined"));
    
    }
    
    private static int seconds;
}

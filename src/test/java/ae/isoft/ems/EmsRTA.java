package ae.isoft.ems;

import org.testng.annotations.Test;

import com.beust.jcommander.Parameter;

import ae.isoft.ems.page.common.LoginPage;
import ae.isoft.ems.page.common.ProfileMenu;
import ae.isoft.ems.util.AutomationLogger;
import ae.isoft.ems.util.Config;
import ae.isoft.ems.util.TestManagedBean;
import ae.isoft.ems.util.TestingWrapper;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.BasicConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

	

public class EmsRTA extends TestManagedBean{
	
	/**
	 * WebDriver Variable
	 */
	JavascriptExecutor jsx = (JavascriptExecutor)driver;	
	
	@Parameters({"username", "password", "mainDptId", "subDpt", "letterType", "file", "letterDescription", "track", "toDesctination",
		"content", "thanksStat", "attachment"})
	@Test
	public void invokeBrowser(String username, String password, String mainDptId, String subDpt, String letterType, String file,
			String letterDescription, String track, String toDesctination, String content, String thanksStat,
			String attachment) throws Exception {
		 
		// Call the page link
		startTest();
		
		//Login page
		LoginPage loginPage = new LoginPage(driver, username, password);
		loginPage.login();
		
		// Open group related to the user
	//	openGroup();
		
		//open right menu 	
		
		ProfileMenu profileMenu = new ProfileMenu(driver);
		// Create outward letter

		profileMenu.openCreateOutward();
				
		
		// Select main dept	
 		if(!TestingWrapper.isBlankOrNull(mainDptId))
		TestingWrapper.selectFromSingleLookup(By.xpath("//*[contains(@id, 'mainDepartment:lookupPanel:component')]/div[1]/div[1]/div[2]/div[1]"),By.xpath("//*[contains(@id, 'mainDepartment:lookupPanel:field-selectized')]"), mainDptId, driver);
		
		// Select sup dept
 		if(!TestingWrapper.isBlankOrNull(subDpt))
		TestingWrapper.selectFromSingleLookup(By.xpath("//*[contains(@id, 'subDepartment:lookupPanel:component')]/div[1]/div[1]/div[2]/div[1]"),
				By.xpath("//*[contains(@id, 'subDepartment:lookupPanel:field-selectized')]"), subDpt, driver);
		
		// Select letter type	
 		if(!TestingWrapper.isBlankOrNull(letterType))
		TestingWrapper.selectFromSingleLookup(By.xpath("//*[contains(@id, 'type:lookupPanel:component')]/div[1]/div[1]/div[2]/div[1]"),
				By.xpath("//*[contains(@id,'type:lookupPanel:field-selectized')]"), letterType, driver);
		
		// Select file
 		if(!TestingWrapper.isBlankOrNull(file))
		TestingWrapper.selectFromSingleLookup(By.xpath("//*[contains(@id, 'file:lookupPanel:component')]/div[1]/div[1]/div[2]/div[1]"),
				By.xpath("//*[contains(@id,'file:lookupPanel:field-selectized')]"), file, driver);
		
		// add letter description
 		if(!TestingWrapper.isBlankOrNull(letterDescription))
		driver.findElement(By.xpath("//*[contains(@id, 'caseDescription:field')]")).sendKeys(letterDescription);
		
		// Scroll Down
		JavascriptExecutor jsx = (JavascriptExecutor)driver;
		jsx.executeScript("window.scrollBy(0,300)", "");
		
		// Select trace
 		if(!TestingWrapper.isBlankOrNull(track))
		TestingWrapper.selectFromSingleLookup(By.xpath("//*[contains(@id, 'workflowPath:component')]/div[1]/div[1]/div[2]/div[1]"),
				By.xpath("//*[contains(@id,'workflowPath:field-selectized')]"), track, driver);
		
		// Click to go to the next page
		
		driver.findElement(By.xpath("//*[contains(@id, 'nextButton:actionButton:component')]/button")).click();
		
		// Select To	
 		if(!TestingWrapper.isBlankOrNull(toDesctination))
		TestingWrapper.selectFromAdvanceLookUp("toDestinations:multiLookupPanel", toDesctination, driver);
		
		// Scroll Down
		jsx.executeScript("window.scrollBy(0,300)", "");
		
		// Add text	
 		if(!TestingWrapper.isBlankOrNull(content)) {
			driver.switchTo().frame(driver.findElement(By.xpath("//*[@id=\"cke_1_contents\"]/iframe"))); 
			driver.findElement(By.xpath("/html/body/p")).click();
			new Actions(driver).moveToElement(driver.findElement(By.xpath("/html/body/p"))).sendKeys(content).perform();
			driver.switchTo().defaultContent();
 		}
		// Scroll Down
		jsx.executeScript("window.scrollBy(0,300)", "");
		
		//Thanks statement
 		if(!TestingWrapper.isBlankOrNull(thanksStat))
		TestingWrapper.selectFromSingleLookup(By.xpath("//*[contains(@id,'thanksStatement:component')]/div[1]/div[1]/div[2]/div[1]"), 
				By.xpath("//*[contains(@id,'thanksStatement:field-selectized')]"), thanksStat, driver);
		
		// Next page
		driver.findElement(By.xpath("//*[contains(@id,'nextButton:actionButton:component')]/button")).click();
		 
		// Add Attachment
 		if(!TestingWrapper.isBlankOrNull(attachment))
        TestingWrapper.uploadFileWithRobot(driver, attachment, "attachments:multiFileUploadPanel:multiLookupPanel", 1);		 
		 // Save button
        TestingWrapper.clickElement(By.xpath("//*[contains(@id,'saveButton:submitButton:actionButton:component')]/button"), driver);
		        
		//switchToSecondWindow();
        TestingWrapper.clickElement(By.xpath("(//*[contains(@id,'confirmButton:actionButton:component')]/button)[2]"), driver);		
		//preview
        TestingWrapper.clickElement(By.xpath("(//*[contains(@id,'previewSavedButton:submitButton:actionButton:component')]/button)[2]"), driver);		
		// Scroll Down
		jsx.executeScript("window.scrollBy(0,300)", "");
		Thread.sleep(5000);
        TestingWrapper.clickElement(By.xpath("//*[contains(@id,'backButton:actionButton:component')]/button"), driver);
		Thread.sleep(3000);

        TestingWrapper.clickElement(By.xpath("//*[contains(@id,'gotoOutboxButton:actionButton:component')]/button"), driver);

         loginPage.signOut();
	}
	
		  		
 	/**
 	 *  Open Group for the selected user
 	 */
 	public void openGroup() throws InterruptedException {
 		try {
 			driver.findElement(By.xpath("//*[contains(@id, 'groupsTable:table')]/div/div[1]/table/tbody/tr[4]")).click();	
 	 		openPosition();
 	 		JavascriptExecutor jsx = (JavascriptExecutor)driver;
 	 		jsx.executeScript("window.scrollBy(0,250)", "");
 	 	    submit();  
			
		} catch (Exception e) {
			// TODO: handle exception
			Thread.sleep(5000);
			driver.findElement(By.xpath("//*[contains(@id, 'groupsTable:table')]/div/div[1]/table/tbody/tr[4]")).click();	
	 		openPosition();
	 		JavascriptExecutor jsx = (JavascriptExecutor)driver;
	 		jsx.executeScript("window.scrollBy(0,250)", "");
	 	    submit();  
		}
   	}
 	
	/**
 	 *  Open Group for the selected user
 	 */
 	public void openPosition() { 
 		
 		driver.findElement(By.cssSelector("#p_818325517\\3a changeUserLoginSettingWidget\\3a form\\3a j_idt700\\3a positionsTable\\3a table > div > div.af_table_data-body > table > tbody > tr")).click();
 	}
 	
	/**
 	 *  submit to login to the page
 	 */
 	public void submit() throws InterruptedException {	  	
		Thread.sleep(1000);	
		driver.findElement(By.xpath("//*[@id=\"p_818325517:changeUserLoginSettingWidget:form:j_idt834:j_idt835:submitButton:actionButton:component\"]/button")).click();
 	}
 	
 	/**
 	 * Open the right menu
 	 */
 	public void rightMenu() throws InterruptedException {
 		TestingWrapper.JSClickElement(By.xpath("//*[@id=\"menu-trigger\"]"), driver);
  	}   
 	

    @BeforeTest
	@Override
	public void beforeTest() {
		chromeInitialize();
		
	}
        
}